# Changelog

## Version 2.1.0

- updated parent dependency
- fixed the `URLAdapter` class, so that it removes the trailing slashes
- fixed an error in the catalog.xml

## Version 2.0.0

- changed the namespaces of some XSD elements
- fixed an error in the unmarshalling method
- updated to the latest parent dependency
- added the first maven site documentation
